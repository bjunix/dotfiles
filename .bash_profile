HISTFILESIZE=1000000
HISTSIZE=100000

[ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion

GIT_PS1_SHOWCOLORHINTS=1
GIT_PS1_SHOWUPSTREAM=1
GIT_PS1_SHOWDIRTYSTATE=1

RESET="\[\017\]"
NORMAL="\[\033[0m\]"
RED="\[\033[31;1m\]"
YELLOW="\[\033[33;1m\]"
WHITE="\[\033[37;1m\]"
SMILEY="${WHITE}:)${NORMAL}"
FROWNY="${RED}:(${NORMAL}"
SELECT="if [ \$? = 0 ]; then echo \"${SMILEY}\"; else echo \"${FROWNY}\"; fi"

export PS1="\[\033[38;5;6m\]\w\[$(tput sgr0)\]\[\033[38;5;15m\] \$(__git_ps1) \[$(tput sgr0)\]\[\033[38;5;1m\]\\$\[$(tput sgr0)\] "

export CLICOLOR=1
export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx


export NVM_DIR="$HOME/.nvm"
. "/usr/local/opt/nvm/nvm.sh"

alias ll="ls -al"

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

PATH=$PATH:~/bin
