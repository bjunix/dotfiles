# Your init script
#
# Atom will evaluate this file each time a new window is opened. It is run
# after packages are loaded/activated and after the previous editor state
# has been restored.
#
# An example hack to log to the console when each text editor is saved.
#
# atom.workspace.observeTextEditors (editor) ->
#   editor.onDidSave ->
#     console.log "Saved! #{editor.getPath()}"

# place this snippet into init.coffee in ~/.atom directory

atom.commands.add 'atom-text-editor',
    'custom:cut-line': (event) ->
        editor = @getModel()
        editor.selectLinesContainingCursors()
        editor.cutSelectedText()

atom.workspace.observeTextEditors (editor) ->
    if editor.getTitle() isnt "untitled"
        sp = editor.getPath().split('/')
        title = sp.slice(sp.length-2).join('/')
        editor.getTitle = -> title

        sp = editor.getPath().split('/')
        sp.splice(0,3) # remove "/Users/<username/" from path
        long_title = sp.join('/')
        editor.getLongTitle = -> long_title

for item in atom.workspace.getPaneItems()
    if item.emitter?
        item.emitter.emit "did-change-title", item.getTitle()
